# search-guard-labs

Search Guard Labs is a place where you can experiment with different Search Guard configurations. 

Read [the docs](docs) to find more about configurations.

## Prerequisites

OS: Linux, FreeBSD or macOS. Libs and programs: Docker, curl and rsync. Internet connection.

## Setup

If necessary, edit .env constants. If you need the data to persist, uncomment the volumes related settings in the docker-compose file.

## Configure

Put Elasticsearch configuration and TLS certificates into the elasticsearch folder, for example `elasticsearch/config/basicauth/es01`.

Or create all configurations automatically
```
./create_config.sh
```

## Run

```
docker-compose -f docker-compose-basicauth.yml up
```

If you want to rebuild:
```
docker-compose -f docker-compose-basicauth.yml up --build
```

## Verify

### Docker
```
docker ps
```

### Elasticsearch
```
curl -k -u admin:admin -X GET https://localhost:9200
curl -k -u admin:admin -X GET https://localhost:9200/_cluster/health?pretty
curl -k -u admin:admin -X GET https://localhost:9200/_cat/indices
```

## Rebuild the Docker images

1. Stop container
2. Find the image `docker image ls`
3. Delete the image, for example `docker image rm search-guard-labs_kibana01`

## Extra

Remove the dangling images: `docker rmi $(docker images -f "dangling=true" -q)`
