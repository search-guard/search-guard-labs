# TLS (Transport Layer Security)

* [Public-key cryptography](#public-key-cryptography)
* [TLS layers](#tls-layers)
   * [Handshake](#handshake)
* [Algorithm](#algorithm)
   * [RSA](#rsa)
      * [Math](#math)
      * [Example](#example)
* [X.509](#x509)
   * [Public key infrastructure (PKI)](#public-key-infrastructure-pki)
* [Create and configure X.509 certificates](#create-and-configure-x509-certificates)
   * [Elasticsearch](#elasticsearch)
      * [Configuration](#configuration)
      * [Create certificates](#create-certificates)
   * [Kibana](#kibana)
      * [Configuration](#configuration-1)
      * [Create certificates](#create-certificates-1)
         * [Root CA private key](#root-ca-private-key)
         * [Root CA certificate](#root-ca-certificate)
         * [Server private key](#server-private-key)
         * [Server certificate signing request (CSR)](#server-certificate-signing-request-csr)
         * [Server certificate extentions](#server-certificate-extentions)
         * [Server certificate](#server-certificate)
* [Troubleshoot certificates](#troubleshoot-certificates)
   * [Verify content](#verify-content)
   * [Verify chain](#verify-chain)
* [References](#references)

## Public-key cryptography
TLS is an implementation of the [public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography) system. The system uses pairs of keys: public keys, which may be available publicly, and private keys, which must be known only by the owner. Anyone can encrypt a message using the public key, but only the owner of the paired private key can decrypt the message.  

![Public-key cryptography](assets/tls/public_key_cryptography.png)

## TLS layers
The TLS protocol comprises two layers:
  1. [TLS record](https://en.wikipedia.org/wiki/Transport_Layer_Security#TLS_record)
  2. [TLS handshake](https://en.wikipedia.org/wiki/Transport_Layer_Security#TLS_handshake)

### Handshake
On start of a connection, the record encapsulates the TLS control protocol (handshake messaging) protocol. The TLS control protocol is used to exchange the information required by the sides before secure data exchange. The format and order of these messages may vary according to the requirements of the client and server.

![TLS handshake](assets/tls/tls_handshake.png)

Legend

| Name | Description |
| ---- | ----------- |
| [Message Authentication Code (MAC)](https://en.wikipedia.org/wiki/Message_authentication_code) | A tag used to authenticate a message. Generated and verified by the common secret. MAC must contain data that assures that it can only be sent once (e.g. time stamp, sequence number or use of a one-time MAC) |

## Algorithm

### RSA

[RSA (Rivest–Shamir–Adleman)](https://simple.wikipedia.org/wiki/RSA_algorithm) is one of [the algorithms](https://en.wikipedia.org/wiki/Transport_Layer_Security#Algorithm) used by TLS for key exchange to encrypt and decrypt data. 

#### Math

[Euler's totient function](https://en.wikipedia.org/wiki/Euler's_totient_function) counts the positive integers up to a given integer $`n`$ that are relatively prime (coprime) to $`n`$. For example, $`φ(8) = 4`$.
Because only the following factors of 8 don't share any factor with it (except 1): $`1, 3, 5, 7`$.

And if $`n`$ is a prime number $`P`$ then $`φ(P) = P - 1`$. For example: $`φ(7) = 6`$ 

The [Euler's totient theorem](https://simple.wikipedia.org/wiki/Euler's_totient_theorem) says if $`m`$ and $`n`$ are coprimes then $`m^{\varphi \left( n\right)  }\equiv 1modn`$

Then we can do some math transformations to get a formula to calculate a secret key.

```math
1^k=1
```

```math
m^{k\times \varphi \left( n\right)  }\equiv 1modn
```

```math
1\times m=m
```

```math
m\times m^{k\times \varphi \left( n\right)  }\equiv mmodn
```

```math
m^{k\times \varphi \left( n\right)+1  }\equiv mmodn
```

```math
m^{ed}\equiv mmodn
```

```math
ed = k\times \varphi \left( n\right)+1
```

```math
d=\frac{k\times \varphi \left( n\right)  +1}{e} 
```

#### Example

![RSA example](assets/tls/rsa_example.png)

**Python**
```python
# Alice
p1 = 23
p2 = 29
n = p1 * p2
phi = (p1 - 1) * (p2 - 1)
e = 3
k = 2
d = (k * phi + 1) / 3
print('Alice, d=', d) # 411
print('Alice, n=', n) # 667
print('Alice, e=', e) # 3

# Bob
m1 = 12
c = (m1**e) % n
print('Bob, m=', m1) # 12
print('Bob, c=', c) # 394

# Alice
m2 = (c**d) % n
print('Alice, m=', m2) # 12
print('Alice, decrypted?', m2 == m1) # True
```

This is how the PreMasterKey can be sent securely. The next step is to use a [pseudorandom function](https://en.wikipedia.org/wiki/Pseudorandom_number_generator), called [(PRF)](https://tools.ietf.org/html/rfc5246#section-6.3), to create the common master key that will be used for symmetric data encryption.

```C
key_block = PRF(pre_master_secret, "key expansion",
  SecurityParameters.server_random + SecurityParameters.client_random);
```

## X.509

[X.509](https://en.wikipedia.org/wiki/X.509) is a standard which specifies a format for public key certificates. It is used in many protocols incuding TLS/SSL.

### Public key infrastructure (PKI)

[PKI](https://en.wikipedia.org/wiki/Public_key_infrastructure) is a set of polices, hardware and software needed to distribute, use, store and revoce certificates. 

![PKI chains](assets/tls/pki_chains.png)

## Create and configure X.509 certificates

### Elasticsearch

#### Configuration

A typical configuration for an Elasticsearch node (basic authentication):
**elasticsearch.yml**
```
node.name: sgssl-0.example.com
network.host: 0.0.0.0
http.port: 9200
cluster.name: es-docker-cluster
discovery.seed_hosts: ["sgssl-1.example.com", "sgssl-2.example.com"]
cluster.initial_master_nodes: ["sgssl-0.example.com", "sgssl-1.example.com", "sgssl-2.example.com"]
bootstrap.memory_lock: true
cluster.routing.allocation.disk.threshold_enabled: false
node.max_local_storage_nodes: 3

xpack.security.enabled: false

searchguard.allow_default_init_sgindex: true
searchguard.audit.type: internal_elasticsearch
searchguard.enable_snapshot_restore_privilege: true
searchguard.check_snapshot_restore_write_privileges: true
searchguard.restapi.roles_enabled: ["SGS_ALL_ACCESS"]

searchguard.ssl.transport.pemcert_filepath: node1.pem
searchguard.ssl.transport.pemkey_filepath: node1.key
searchguard.ssl.transport.pemkey_password: QxoQyHTsXipE
searchguard.ssl.transport.pemtrustedcas_filepath: root-ca.pem
searchguard.ssl.transport.enforce_hostname_verification: false
searchguard.ssl.transport.resolve_hostname: false
searchguard.ssl.http.enabled: true
searchguard.ssl.http.pemcert_filepath: node1_http.pem
searchguard.ssl.http.pemkey_filepath: node1_http.key
searchguard.ssl.http.pemkey_password: AqrumRQqSIqo
searchguard.ssl.http.pemtrustedcas_filepath: root-ca.pem
searchguard.nodes_dn:
- CN=node1.example.com,OU=Ops,O=Example Com\, Inc.,DC=example,DC=com
- CN=node2.example.com,OU=Ops,O=Example Com\, Inc.,DC=example,DC=com
- CN=node3.example.com,OU=Ops,O=Example Com\, Inc.,DC=example,DC=com
searchguard.authcz.admin_dn:
- CN=kirk.example.com,OU=Ops,O=Example Com\, Inc.,DC=example,DC=com
```

Transport options. They are to ensure secure inter-node communication.

| Name | Description |
| ---- | ----------- |
| searchguard.ssl.transport.pemcert_filepath | Path to the X.509 node certificate chain (PEM format), which must be under the config/ directory, specified using a relative path (mandatory) |
| searchguard.ssl.transport.pemkey_filepath | Path to the certificates key file (PKCS #8), which must be under the config/ directory, specified using a relative path (mandatory) |
| searchguard.ssl.transport.pemkey_password | Key password. Omit this setting if the key has no password. (optional) |
| searchguard.ssl.transport.pemtrustedcas_filepath | Path to the root CA(s) (PEM format), which must be under the config/ directory, specified using a relative path (mandatory) |
| searchguard.ssl.transport.enforce_hostname_verification | Whether or not to verify hostnames on the transport layer. (Optional, default: true) |
| searchguard.ssl.transport.resolve_hostname | Whether or not to resolve hostnames against DNS on the transport layer. (Optional, default: true, only works if hostname verification is enabled.) |
| searchguard.nodes_dn | To identify inter-cluster requests reliably. Search Guard supports wildcards and regular expressions. If your node certificates have an [object identifier (OID)](https://en.wikipedia.org/wiki/Object_identifier) in the Subject Alternative Name (SAN) section, you can omit this configuration completely. [Read more about using Search Guard TLS settings in production.](https://docs.search-guard.com/latest/tls-in-production). |

REST API options. They are to ensure secure access to REST API.

| Name | Description |
| ---- | ----------- |
| searchguard.ssl.http.enabled | Whether to enable TLS on the REST layer or not. If enabled, only HTTPS is allowed. (Optional, default: false) |
| searchguard.ssl.http.pemcert_filepath | Path to the X.509 node certificate chain (PEM format), which must be under the config/ directory, specified using a relative path (mandatory) | 
| searchguard.ssl.http.pemkey_filepath | Path to the certificates key file (PKCS #8), which must be under the config/ directory, specified using a relative path (mandatory) |
| searchguard.ssl.http.pemkey_password | Key password. Omit this setting if the key has no password. (optional) |
| searchguard.ssl.http.pemtrustedcas_filepath | Path to the root CA(s) (PEM format), which must be under the config/ directory, specified using a relative path (mandatory) |
| searchguard.authcz.admin_dn | Admin certificates are regular client certificates that have elevated rights to perform administrative tasks. You need an admin certificate to change the Search Guard configuration via the sgadmin command line tool, or to use the REST management API. For security reasons, you cannot use wildcards or regular expressions here. |

#### Create certificates
We can use Search Guard TLS tool to generate certificate for an Elasticsearch cluster of three nodes.

```bash
wget https://releases.floragunn.com/search-guard-tlstool/1.8/search-guard-tlstool-1.8.zip
unzip search-guard-tlstool-1.8.zip -d search-guard-tlstool
```

Edit **example.yml** if you need to customize the certificates options. Crete the certificates:
```bash
cd search-guard-tlstool
./tools/sgtlstool.sh -c config/example.yml -ca -crt
```

Find the certificates in the **out/** folder. Now you must copy the certificates to the respective nodes.
```bash
$ for n in search-guard-tlstool/out/*; do printf '%s\n' "$n"; done
search-guard-tlstool/out/client-certificates.readme
search-guard-tlstool/out/kirk.key
search-guard-tlstool/out/kirk.pem
search-guard-tlstool/out/node1.key
search-guard-tlstool/out/node1.pem
search-guard-tlstool/out/node1_elasticsearch_config_snippet.yml
search-guard-tlstool/out/node1_http.key
search-guard-tlstool/out/node1_http.pem
search-guard-tlstool/out/node2.key
search-guard-tlstool/out/node2.pem
search-guard-tlstool/out/node2_elasticsearch_config_snippet.yml
search-guard-tlstool/out/node2_http.key
search-guard-tlstool/out/node2_http.pem
search-guard-tlstool/out/node3.key
search-guard-tlstool/out/node3.pem
search-guard-tlstool/out/node3_elasticsearch_config_snippet.yml
search-guard-tlstool/out/node3_http.key
search-guard-tlstool/out/node3_http.pem
search-guard-tlstool/out/root-ca.key
search-guard-tlstool/out/root-ca.pem
search-guard-tlstool/out/root-ca.readme
search-guard-tlstool/out/signing-ca.key
search-guard-tlstool/out/signing-ca.pem
search-guard-tlstool/out/spock.key
search-guard-tlstool/out/spock.pem
```

### Kibana

To create a certificate that can be used in a web browser we need:
  1. Create a X.509 certificate for Kibana and sign it by the Signing CA intermediate certificate.
  2. Configure Kibana to use the certificates.

#### Configuration
A typical configuration for Kibana (basic authentication).
**kibana.yml**
```
server.host: "0.0.0.0"
elasticsearch.hosts: ["https://sgssl-0.example.com:9200"]
elasticsearch.username: "kibanaserver"
elasticsearch.password: "kibanaserver"
elasticsearch.ssl.verificationMode: none
# elasticsearch.ssl.certificateAuthorities: ["./config/root-ca.pem"]
# elasticsearch.ssl.verificationMode: full
elasticsearch.requestHeadersWhitelist: [ "authorization", "sgtenant" ]

xpack.security.enabled: false

server.ssl.enabled: true
server.ssl.certificate: ./config/kibana.example.com.crt
server.ssl.key: ./config/kibana.example.com.key

searchguard.multitenancy.enabled: true
searchguard.accountinfo.enabled: true
```

Options:

| Name | Description |
| ---- | ----------- |
| elasticsearch.hosts | A list of Elasticsearch hosts. You must set HTTPS protocol for all hosts. |
| elasticsearch.username | Kibana internal server user username. The user is required to perform maintenance on indices during startup phase. | 
| elasticsearch.password | Kibana internal server user password. |
| elasticsearch.ssl.certificateAuthorities | Paths to CA certificates. This used to establish trust when making outbound TLS connections to Elasticsearch. |
| elasticsearch.ssl.verificationMode | Validation of the server certificate that Kibana receives when making outbound TLS connection to Elasticsearch. The options: "none", "full" and "certificate" (skips hostname verification). |
| elasticsearch.requestHeadersWhitelist | A list of Kibana UI headers to send to Elasticsearch. |
| server.ssl.enabled | Enable or disable TLS. |
| server.ssl.certificate | Path to TLS certificate. |
| server.ssl.key | Path to private key. |

#### Create certificates

##### Root CA private key

```bash
openssl genrsa -out root-ca.key -passout pass:foobar 2048
```

| Name | Description |
| ---- | ----------- |
| genrsa | Generate RSA private key. |
| -out file | The output file to write the certificate. |
| -passout arg | The key password source. | 
| 2048 | The size of the private key in bits. |

##### Root CA certificate

```bash
openssl req -x509 -new \
  -key root-ca.key \
  -sha256 \
  -days 1095 \
  -out root-ca.pem \
  -passin pass:foobar \
  -subj "/C=DE/ST=Neverland/L=Neverland/O=CA/CN=ca.com"
```

| Name | Description |
| ---- | ----------- |
| -x509 | Create X.509 certificate. |
| -sha256 | The protocol for the message digest to sign the request with. |
| -days number | The number of days to make the certificate valid for. |
| -out file | The output file to write the certificate. |
| -passin arg | The key password source. | 
| -subj string | Replaces the subject field of an input request with specified data. |

##### Server private key
Create a private key
```bash
openssl genrsa -out kibana.example.com.key -passout pass:foobar 2048
```

Options:

| Name | Description |
| ---- | ----------- |
| genrsa | Generate RSA private key. |
| -out file | The output file to write the certificate. |
| -passout arg | The key password source. | 
| 2048 | The size of the private key in bits. |

##### Server certificate signing request (CSR)
Create a CSR. It is required to obtain the certificate from the CA.
```bash
openssl req -new \
  -key kibana.example.com.key \
  -out kibana.example.com.csr \
  -passin pass:foobar \
  -subj "/C=DE/ST=Neverland/L=Neverland/O=Client/CN=kibana.example.com"
```

Options:

| Name | Description |
| ---- | ----------- |
| req -new | Create a CSR. |
| -key file | The file to read the private key from. |
| -out file | The output file to write the CSR. |
| -passin arg | The key password source. | 
| -subj string | Replaces the subject field of an input request with specified data. |

##### Server certificate extentions
Create a configuration file to read certificate extensions from.

**kibana.example.com.ext**
```
extendedKeyUsage=serverAuth,clientAuth
```

Options:

| Name | Description |
| ---- | ----------- |
| extendedKeyUsage | A list of usages indicating purposes for which the certificate can be used for. | 

##### Server certificate
Create the certificate signed by CA.
```bash
openssl x509 -req \
  -in kibana.example.com.csr \
  -CA root-ca.pem \
  -CAkey root-ca.key \
  -CAcreateserial \
  -out kibana.example.com.crt \
  -days 1095 \
  -sha256 \
  -extfile kibana.example.com.ext \
  -passin pass:foobar
```

Options:

| Name | Description |
| ---- | ----------- |
| x509 | Create X.509 certificate. |
| -req | Expect the CSR. |
| -in file | The input file. |
| -CA file | The CA certificate to be used for signing. |
| -CAkey file | The CA private key to sign the certificate. |
| -CAcreateserial | Create the CA serial number file if it does not exist instead of generating an error. |
| -out file | The output file to write the certificate. |
| -days number | The number of days to make the certificate valid for. |
| -extfile file | The containing certificate extensions to use. |
| -passin arg | The key password source. | 

## Troubleshoot certificates

### Verify content
The content of a PEM certificate can either be displayed by using openssl. 

```bash
$ openssl x509 -in node1.pem -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1590160957777 (0x1723cfaa151)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: DC=com, DC=example, O=Example Com, Inc., OU=CA, CN=signing.ca.example.com
        Validity
            Not Before: May 22 15:22:39 2020 GMT
            Not After : May 20 15:22:39 2030 GMT
        Subject: DC=com, DC=example, O=Example Com, Inc., OU=Ops, CN=node1.example.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:b2:57:bb:8c:f8:2e:d8:0a:5b:69:53:86:a2:92:
                    60:6c:46:30:44:26:10:5b:e0:b6:13:94:45:27:c4:
                    27:a2:69:ec:db:53:1e:61:b9:6e:16:50:19:ae:04:
                    36:98:65:a7:d7:60:28:7b:99:00:66:2d:b3:9b:76:
                    58:b9:9b:86:f2:0c:ec:9a:4d:f9:3a:84:bf:e3:25:
                    e3:12:44:86:f7:fd:d4:63:c6:53:21:8b:8f:a6:c3:
                    88:75:b5:18:18:01:00:04:a1:e2:81:a7:80:f0:e5:
                    59:18:72:f9:16:ac:34:98:3b:be:2b:23:a3:c8:f6:
                    39:57:0e:83:08:c9:d6:30:2f:0c:8b:ea:3c:2e:8c:
                    11:fc:2c:fa:8b:74:4d:0e:af:68:3b:ea:ee:ab:6c:
                    5c:c2:44:9d:88:2b:95:ad:4a:41:02:8f:77:93:69:
                    24:48:27:cc:5c:6f:78:3f:c3:f7:2f:c9:91:59:ca:
                    9e:12:ce:96:bb:2f:e1:d3:d7:02:cc:12:aa:4e:3a:
                    ac:82:e9:d1:5c:91:70:49:44:cb:86:fa:ab:d8:20:
                    ef:7a:b6:32:9a:41:1e:b8:a8:65:75:30:ea:c7:ba:
                    86:30:3a:26:a7:46:34:4a:ff:b8:29:f9:8a:0e:f1:
                    f5:fc:a2:e9:df:de:33:d3:33:b0:a0:f8:d0:89:01:
                    38:3d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Authority Key Identifier: 
                keyid:68:26:81:65:E7:44:A9:D1:DD:AC:1E:88:16:B9:9A:FF:52:01:A4:0E
                DirName:/DC=com/DC=example/O=Example Com, Inc./OU=CA/CN=root.ca.example.com
                serial:02

            X509v3 Subject Key Identifier: 
                BD:7F:05:16:D2:71:83:16:4F:81:FE:22:C4:74:22:9B:8B:6D:BA:DE
            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Key Usage: critical
                Digital Signature, Non Repudiation, Key Encipherment
            X509v3 Extended Key Usage: critical
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Subject Alternative Name: 
                DNS:node1.example.com, IP Address:10.0.2.1
    Signature Algorithm: sha256WithRSAEncryption
         59:15:33:6b:47:95:b9:aa:fa:47:b9:a0:46:24:bc:e3:5e:bb:
         4b:fa:3f:1a:1d:3a:5a:c4:cc:e1:e6:67:c6:d7:3b:73:7f:f1:
         43:89:f2:e0:4b:e6:99:65:ee:bc:c8:2d:98:b4:89:4c:22:2c:
         ec:9a:8a:f6:3c:a8:39:30:9a:c5:3d:f9:38:86:5d:19:f5:88:
         1f:ea:22:cf:e1:ef:f8:92:bc:1d:c1:2c:81:a0:20:3c:65:fd:
         0a:9f:6c:0d:31:25:6f:c1:9d:8f:45:3a:97:b9:c0:12:84:5a:
         d4:89:da:53:56:b0:47:1f:9e:3c:20:96:41:fb:6b:6e:a1:ec:
         c2:3f:92:49:01:23:ea:44:9a:62:f7:3b:42:04:73:f7:51:e8:
         03:95:02:9b:3e:1a:04:a4:73:a4:a3:33:8c:67:bf:68:21:bd:
         b6:24:cc:c4:bd:0d:bd:6d:c8:9c:87:dc:7f:f4:c5:63:fa:e3:
         7b:68:28:67:49:7e:62:4e:e7:57:74:e8:fb:3f:29:b9:f0:92:
         9e:19:ec:48:e1:3b:93:60:3e:df:06:30:a8:b0:ca:83:8b:ee:
         a1:10:19:3a:12:12:71:45:e3:96:c3:41:ba:cb:8d:18:ff:2b:
         0b:f4:83:e4:21:77:fe:ad:6a:4c:20:64:81:56:8f:ec:54:a0:
         2f:29:3f:61
-----BEGIN CERTIFICATE-----
MIIElzCCA3+gAwIBAgIGAXI8+qFRMA0GCSqGSIb3DQEBCwUAMHgxEzARBgoJkiaJ
k/IsZAEZFgNjb20xFzAVBgoJkiaJk/IsZAEZFgdleGFtcGxlMRowGAYDVQQKDBFF
eGFtcGxlIENvbSwgSW5jLjELMAkGA1UECwwCQ0ExHzAdBgNVBAMMFnNpZ25pbmcu
Y2EuZXhhbXBsZS5jb20wHhcNMjAwNTIyMTUyMjM5WhcNMzAwNTIwMTUyMjM5WjB0
MRMwEQYKCZImiZPyLGQBGRYDY29tMRcwFQYKCZImiZPyLGQBGRYHZXhhbXBsZTEa
MBgGA1UECgwRRXhhbXBsZSBDb20sIEluYy4xDDAKBgNVBAsMA09wczEaMBgGA1UE
AwwRbm9kZTEuZXhhbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQCyV7uM+C7YCltpU4aikmBsRjBEJhBb4LYTlEUnxCeiaezbUx5huW4WUBmu
BDaYZafXYCh7mQBmLbObdli5m4byDOyaTfk6hL/jJeMSRIb3/dRjxlMhi4+mw4h1
tRgYAQAEoeKBp4Dw5VkYcvkWrDSYO74rI6PI9jlXDoMIydYwLwyL6jwujBH8LPqL
dE0Or2g76u6rbFzCRJ2IK5WtSkECj3eTaSRIJ8xcb3g/w/cvyZFZyp4Szpa7L+HT
1wLMEqpOOqyC6dFckXBJRMuG+qvYIO96tjKaQR64qGV1MOrHuoYwOianRjRK/7gp
+YoO8fX8ounf3jPTM7Cg+NCJATg9AgMBAAGjggEpMIIBJTCBnwYDVR0jBIGXMIGU
gBRoJoFl50Sp0d2sHogWuZr/UgGkDqF5pHcwdTETMBEGCgmSJomT8ixkARkWA2Nv
bTEXMBUGCgmSJomT8ixkARkWB2V4YW1wbGUxGjAYBgNVBAoMEUV4YW1wbGUgQ29t
LCBJbmMuMQswCQYDVQQLDAJDQTEcMBoGA1UEAwwTcm9vdC5jYS5leGFtcGxlLmNv
bYIBAjAdBgNVHQ4EFgQUvX8FFtJxgxZPgf4ixHQim4ttut4wDAYDVR0TAQH/BAIw
ADAOBgNVHQ8BAf8EBAMCBeAwIAYDVR0lAQH/BBYwFAYIKwYBBQUHAwEGCCsGAQUF
BwMCMCIGA1UdEQQbMBmCEW5vZGUxLmV4YW1wbGUuY29thwQKAAIBMA0GCSqGSIb3
DQEBCwUAA4IBAQBZFTNrR5W5qvpHuaBGJLzjXrtL+j8aHTpaxMzh5mfG1ztzf/FD
ifLgS+aZZe68yC2YtIlMIizsmor2PKg5MJrFPfk4hl0Z9Ygf6iLP4e/4krwdwSyB
oCA8Zf0Kn2wNMSVvwZ2PRTqXucAShFrUidpTVrBHH548IJZB+2tuoezCP5JJASPq
RJpi9ztCBHP3UegDlQKbPhoEpHOkozOMZ79oIb22JMzEvQ29bcich9x/9MVj+uN7
aChnSX5iTudXdOj7Pym58JKeGexI4TuTYD7fBjCosMqDi+6hEBk6EhJxReOWw0G6
y40Y/ysL9IPkIXf+rWpMIGSBVo/sVKAvKT9h
-----END CERTIFICATE-----
```

The important fields to check:

| Name | Description |
| ---- | ----------- |
| Issuer | Issuer distinguished name (DN). |
| Validity | Verify whether the certificate is not expired. |
| Subject | Subject DN. |
| X509v3 extensions | It is critical to check the usage and constraints. The certificate is invalid if usage or constrants are wrong. Also, you can define alternative DNS names and IP addresses if needed. |

### Verify chain

```
$ openssl verify search-guard-tlstool/out/node1.pem 
search-guard-tlstool/out/node1.pem: DC = com, DC = example, O = "Example Com, Inc.", OU = Ops, CN = node1.example.com
error 20 at 0 depth lookup:unable to get local issuer certificate
```

If you see the error above it means this certificate CA is not in your system's root keychain. To verify it you should specify the CA certificate. For example:
```
$ openssl verify -CAfile root-ca.pem -untrusted signing-ca.pem node1.pem 
node1.pem: OK
```

Omit the intermediate certificate if it was not used to issue the certificate you are veryfying.
```
$ openssl verify -CAfile ca.pem cert.pem
cert.pem: OK
```

Read more about [SearchGuard TLS troubleshooting](https://docs.search-guard.com/latest/troubleshooting-tls#tls-troubleshooting).

## References
  * [Search Guard documentation](https://docs.search-guard.com/latest/configuring-tls)
  * [Khan Academy, RSA](https://www.khanacademy.org/computing/computer-science/cryptography/modern-crypt/v/intro-to-rsa-encryption)
  * [Internet X.509 Public Key Infrastructure Certificate and Certificate Revocation List (CRL) Profile](https://tools.ietf.org/html/rfc5280)
  * [OpenSSL manual](https://www.openssl.org/docs/manmaster/man1/openssl.html)
  * [Configuring Kibana](https://www.elastic.co/guide/en/kibana/current/settings.html)
  * [Configuring Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/settings.html)
