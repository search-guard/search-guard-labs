#!/bin/bash

set -e

AUTH="$1"
TLS_TOOL_VERSION="1.8"

if [ -z "$AUTH" ]; then
  AUTH="basicauth"
fi

if [[ "$AUTH" != "basicauth" ]]; then
  echo "Unknown command: $AUTH"
  echo
  echo "Usage:"
  echo "./create_config.sh basicauth           Create TLS certificates for the basicauth."
  exit 1
fi

create_elasticsearch_tls_certificates() {
  rm -rf search-guard-tlstool*
  wget https://maven.search-guard.com/search-guard-tlstool/1.8/search-guard-tlstool-$TLS_TOOL_VERSION.zip
  unzip search-guard-tlstool-$TLS_TOOL_VERSION.zip -d search-guard-tlstool
  cd search-guard-tlstool
  ./tools/sgtlstool.sh -c config/example.yml -ca -crt
  cd -
  rsync -av search-guard-tlstool/out/ elasticsearch/config/$AUTH/sgssl-0.example.com/ --exclude="node[23]*"
  rsync -av search-guard-tlstool/out/ elasticsearch/config/$AUTH/sgssl-1.example.com/ --exclude="node[13]*"
  rsync -av search-guard-tlstool/out/ elasticsearch/config/$AUTH/sgssl-2.example.com/ --exclude="node[12]*"
}

create_kibana_tls_certificate() {
  KIBANA_DOMAIN="kibana.example.com"
  SIGNING_CA="signing-ca"
  ROOT_CA="root-ca"

  rsync -av search-guard-tlstool/out/ kibana/config/$AUTH/kibana.example.com --include="$SIGNING_CA*" --include="$ROOT_CA*" --exclude="*"
  cd kibana/config/$AUTH/kibana.example.com

  if [[
    ! -f $SIGNING_CA.pem ||
    ! -f $SIGNING_CA.key ||
    ! -f $ROOT_CA.readme
  ]]; then
    echo "ERROR: Required files: $SIGNING_CA.pem, $SIGNING_CA.key, $ROOT_CA.readme"
    exit 1
  fi

  openssl genrsa -out $KIBANA_DOMAIN.key 2048

  openssl req \
    -new -key $KIBANA_DOMAIN.key \
    -out $KIBANA_DOMAIN.csr \
    -subj "/C=DE/ST=Neverland/L=Neverland/O=Client/CN=$KIBANA_DOMAIN"

  SIGNING_CA_PASS=$(grep intermediate $ROOT_CA.readme -A 1 | tail -1 | tr -d '[:blank:]' | awk -F':' '{print $2}')
  if [[ -z $SIGNING_CA_PASS ]]; then
    echo "ERROR: Failed to find $SIGNING_CA password in $ROOT_CA.readme"
    exit 1
  fi

    cat > $KIBANA_DOMAIN.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage=serverAuth,clientAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = $KIBANA_DOMAIN
DNS.2 = bar.$KIBANA_DOMAIN
EOF

  openssl x509 -req \
    -in $KIBANA_DOMAIN.csr \
    -CA $SIGNING_CA.pem \
    -CAkey $SIGNING_CA.key \
    -CAcreateserial \
    -out $KIBANA_DOMAIN.crt \
    -days 1825 \
    -sha256 \
    -extfile $KIBANA_DOMAIN.ext \
    -passin pass:$SIGNING_CA_PASS
  cd -
}

create_basicauth_sgconfig_config() {
  # ES01
  cd elasticsearch/config/$AUTH/sgssl-0.example.com/sgconfig
  cat sg_action_groups.yml.example > sg_action_groups.yml
  cat sg_blocks.yml.example > sg_blocks.yml
  cat sg_config.yml.example > sg_config.yml
  cat sg_internal_users.yml.example > sg_internal_users.yml
  cat sg_roles.yml.example > sg_roles.yml
  cat sg_roles_mapping.yml.example > sg_roles_mapping.yml
  cat sg_tenants.yml.example > sg_tenants.yml
  cd -
  
  # ES02
  cd elasticsearch/config/$AUTH/sgssl-1.example.com/sgconfig
  cat sg_action_groups.yml.example > sg_action_groups.yml
  cat sg_blocks.yml.example > sg_blocks.yml
  cat sg_config.yml.example > sg_config.yml
  cat sg_internal_users.yml.example > sg_internal_users.yml
  cat sg_roles.yml.example > sg_roles.yml
  cat sg_roles_mapping.yml.example > sg_roles_mapping.yml
  cat sg_tenants.yml.example > sg_tenants.yml
  cd -
  
  # ES03
  cd elasticsearch/config/$AUTH/sgssl-2.example.com/sgconfig
  cat sg_action_groups.yml.example > sg_action_groups.yml
  cat sg_blocks.yml.example > sg_blocks.yml
  cat sg_config.yml.example > sg_config.yml
  cat sg_internal_users.yml.example > sg_internal_users.yml
  cat sg_roles.yml.example > sg_roles.yml
  cat sg_roles_mapping.yml.example > sg_roles_mapping.yml
  cat sg_tenants.yml.example > sg_tenants.yml
  cd -
}

create_basicauth_elasticsearch_config() {
  # ES01
  cd elasticsearch/config/$AUTH/sgssl-0.example.com
  cat > elasticsearch.yml << EOF
node.name: sgssl-0.example.com
network.host: 0.0.0.0
http.port: 9200
cluster.name: es-docker-cluster
discovery.seed_hosts: ["sgssl-1.example.com", "sgssl-2.example.com"]
cluster.initial_master_nodes: ["sgssl-0.example.com", "sgssl-1.example.com", "sgssl-2.example.com"]
bootstrap.memory_lock: true
cluster.routing.allocation.disk.threshold_enabled: false
node.max_local_storage_nodes: 3

xpack.security.enabled: false

searchguard.allow_default_init_sgindex: true
searchguard.audit.type: internal_elasticsearch
searchguard.enable_snapshot_restore_privilege: true
searchguard.check_snapshot_restore_write_privileges: true
searchguard.restapi.roles_enabled: ["SGS_ALL_ACCESS"]
EOF
  cat node1_elasticsearch_config_snippet.yml >> elasticsearch.yml 
  cd -
  
  # ES02
  cd elasticsearch/config/$AUTH/sgssl-1.example.com
  cat > elasticsearch.yml << EOF
node.name: sgssl-1.example.com
network.host: 0.0.0.0
http.port: 9200
cluster.name: es-docker-cluster
discovery.seed_hosts: ["sgssl-0.example.com", "sgssl-2.example.com"]
cluster.initial_master_nodes: ["sgssl-0.example.com", "sgssl-1.example.com", "sgssl-2.example.com"]
bootstrap.memory_lock: true
cluster.routing.allocation.disk.threshold_enabled: false
node.max_local_storage_nodes: 3

xpack.security.enabled: false

searchguard.allow_default_init_sgindex: true
searchguard.audit.type: internal_elasticsearch
searchguard.enable_snapshot_restore_privilege: true
searchguard.check_snapshot_restore_write_privileges: true
searchguard.restapi.roles_enabled: ["SGS_ALL_ACCESS"]
EOF
  cat node2_elasticsearch_config_snippet.yml >> elasticsearch.yml 
  cd -
  
  # ES03
  cd elasticsearch/config/$AUTH/sgssl-2.example.com
  cat > elasticsearch.yml << EOF
node.name: sgssl-2.example.com
network.host: 0.0.0.0
http.port: 9200
cluster.name: es-docker-cluster
discovery.seed_hosts: ["sgssl-0.example.com", "sgssl-1.example.com"]
cluster.initial_master_nodes: ["sgssl-0.example.com", "sgssl-1.example.com", "sgssl-2.example.com"]
bootstrap.memory_lock: true
cluster.routing.allocation.disk.threshold_enabled: false
node.max_local_storage_nodes: 3

xpack.security.enabled: false

searchguard.allow_default_init_sgindex: true
searchguard.audit.type: internal_elasticsearch
searchguard.enable_snapshot_restore_privilege: true
searchguard.check_snapshot_restore_write_privileges: true
searchguard.restapi.roles_enabled: ["SGS_ALL_ACCESS"]
EOF
  cat node3_elasticsearch_config_snippet.yml >> elasticsearch.yml 
  cd -
}

create_basicauth_kibana_config() {
  cp search-guard-tlstool/out/root-ca.pem kibana/config/$AUTH/kibana.example.com
  cd kibana/config/$AUTH/kibana.example.com
  cat > kibana.yml << EOF
server.host: "0.0.0.0"
elasticsearch.hosts: ["https://sgssl-0.example.com:9200"]
elasticsearch.username: "kibanaserver"
elasticsearch.password: "kibanaserver"
elasticsearch.ssl.verificationMode: none
# elasticsearch.ssl.certificateAuthorities: ["./config/root-ca.pem"]
# elasticsearch.ssl.verificationMode: full
elasticsearch.requestHeadersWhitelist: [ "authorization", "sgtenant" ]

xpack.security.enabled: false

server.ssl.enabled: true
server.ssl.certificate: ./config/kibana.example.com.crt
server.ssl.key: ./config/kibana.example.com.key

searchguard.multitenancy.enabled: true
searchguard.accountinfo.enabled: true
EOF
  cd -
}

main() {
  create_elasticsearch_tls_certificates
  create_kibana_tls_certificate

  if [[ "$AUTH" == "basicauth" ]]; then
    create_basicauth_elasticsearch_config
    create_basicauth_sgconfig_config
    create_basicauth_kibana_config
  fi

  echo
  echo
  echo "Success!!!"
}

main
